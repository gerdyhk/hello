import { useState, useEffect } from 'react'

const VALIDATE_OBJ = {
  name: {
    message: 'please input at least 3 characters',
    pattern: '^[\\w]{3,}$',
    required: true,
  },
  email: {
    message: 'please input the vadation email format',
    pattern: '^\\w[\\w.]*@\\w[\\w.]*\\.\\w+$',
    required: true,
    relatedTo: 'confirmEmail',
  },
  confirmEmail: {
    message: 'please input the email again',
    pattern: '^\\w[\\w.]*@\\w[\\w.]*\\.\\w+$',
    required: true,
    relatedTo: 'email',
  },
}
/**
 * @desc 封装了表单的相关逻辑,formData为填写表单后的数据，useInputMethod为提供input所需要的参数的方法
 * @returns [Array] 返回值为 [formData,useInputMethod]
 */
export default function useForm() {
  const [formData, setDate] = useState({})
  /**
   * @desc 根据name值，构造出该input所需要的属性参数
   * @param {string} name input的name值
   */
  function useInputMethod(name) {
    const [inputObj, setState] = useState({ text: '' })
    const { text } = inputObj
    const {
      message, pattern, required, relatedTo,
    } = VALIDATE_OBJ[name] || {}
    const prop = {
      onChange: event => {
        const { value, validity } = event.target
        let isValid = !validity.patternMismatch
        if (isValid && relatedTo) {
          isValid = value === formData[relatedTo].text
        }
        if (relatedTo) {
          formData[relatedTo].isValid = isValid
        }
        if (!isValid) {
          event.target.setCustomValidity(message)
        } else {
          event.target.setCustomValidity('')
        }

        return setState({
          text: value,
          isValid,
        })
      },
      value: text,
      name,
      pattern,
      required,
    }
    useEffect(() => {
      formData[name] = inputObj
      setDate(formData)
    })
    return prop
  }
  return [formData, useInputMethod]
}
