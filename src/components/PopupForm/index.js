import React, { useState } from 'react'
import useForm from './validate'
import styles from './index.styl'
import { submitService } from '../../services/home'


function PopupForm(isPopup, setPopup) {
  /**
   * @desc 关闭弹层
   */
  const closePopup = () => {
    setPopup(false)
  }
  // formData 为 表单里的数据汇总
  const [formData, useInputMethod] = useForm()
  // isShowSuccessTip 为真时，显示提交成功的弹层
  const [isShowSuccessTip, useShowSuccessTip] = useState(false)
  // errorMessage 为提交失败后，显示的错误信息
  const [errorMessage, useUpdateErrorMessage] = useState('')
  const [submitObj, useSubmitMethod] = useState({
    text: '提交',
    addLoadingClass: false,
  })
  /**
   *
   * @param {boolean} isLoading
   */
  const submitStatusChange = isLoading => {
    if (isLoading) {
      useSubmitMethod({
        text: 'Sending, please wait...',
        addLoadingClass: true,
      })
      return
    }
    useSubmitMethod({
      text: 'Send',
      addLoadingClass: false,
    })
  }
  const submitMethod = ev => {
    let isAllValid = true
    Object.keys(formData).forEach(key => {
      const { isValid } = formData[key]
      if (!isValid) {
        isAllValid = false
      }
    })
    if (!isAllValid) {
      return
    }
    ev.preventDefault();
    const { name, email } = formData
    submitStatusChange(true)
    useUpdateErrorMessage('')
    submitService({
      name: name.text,
      email: email.text,
    }).then(() => {
      submitStatusChange(false)
      useShowSuccessTip(true)
    }).catch(err => {
      useUpdateErrorMessage(err.message)
      submitStatusChange(false)
    })
  }

  const wrapperClick = e => {
    if (e.target.dataset.wrapper) {
      closePopup()
    }
  }

  const renderFormSection = isShowTip => {
    const formClassArr = [styles.formWrapper]
    if (!isShowTip) {
      formClassArr.push(styles.isBlock)
    }
    const buttonClassName = [styles.formItem]
    const { text, addLoadingClass } = submitObj
    if (addLoadingClass) {
      buttonClassName.push(styles.loading)
    }
    return (
      <form className={formClassArr.join(' ')}>
        <dl>
          <dt className={styles.dtWrapper}>Request an invite</dt>
          <dd className={styles.ddWrapper}>
            <input type="text" {...useInputMethod('name')} placeholder="Full name" />
          </dd>
          <dd className={styles.ddWrapper}>
            <input type="text" {...useInputMethod('email')} placeholder="Email" />
          </dd>
          <dd className={styles.ddWrapper}>
            <input type="text" {...useInputMethod('confirmEmail')} placeholder="Confirm email" required />
          </dd>
        </dl>
        <button className={buttonClassName.join(' ')} type="submit" onClick={submitMethod}>{text}</button>
        {!!errorMessage && <span className={styles.errorMessage}>{errorMessage}</span>}
      </form>
    )
  }
  const renderSuccessTipSection = isShowTip => {
    const tipClassArr = [styles.tipWrapper]
    if (isShowTip) {
      tipClassArr.push(styles.isFlex)
    }
    return (
      <div className={tipClassArr.join(' ')}>
        <h2 className={styles.tipItem}>All done!</h2>
        <p className={styles.tipItem}>
You will be one of the first
      to experience Broccoli & Co. when we launch.
        </p>
        <button type="button" className={styles.tipOk} onClick={closePopup}>OK</button>
      </div>
    )
  }

  const wrapperClassName = [styles.wrapper]
  if (isPopup) {
    wrapperClassName.push(styles.isFlex)
  }
  return (
    <section key="5" className={wrapperClassName.join(' ')} onClick={wrapperClick} data-wrapper>
      {renderSuccessTipSection(isShowSuccessTip)}
      {renderFormSection(isShowSuccessTip)}
    </section>
  )
}

export default PopupForm
