import React from 'react'
import styles from './index.styl'

const Header = () => <div className={styles.header}>BROCCOLI & CO.</div>

export default Header
