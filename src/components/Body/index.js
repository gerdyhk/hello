import React from 'react'
import styles from './index.styl'

const Body = setPopup => {
  const showModal = () => {
    setPopup(true)
  }
  return (
    <section className={styles.bodyWrapper}>
      <h1>A better way to enjoy every day.</h1>
      <p>Be the first to know when we launch.</p>
      <button type="button" onClick={showModal}>Request an invite</button>
    </section>
  )
}

export default Body
