import React from 'react'
import styles from './index.styl'

const Footer = () => (
  <div className={styles.footer}>
    <p>
        Made with
      <span role="img" aria-label="heart">❤️</span>
      {' '}
        in Melbourne.
    </p>
    <p>© 2016 Broccoli & Co. All rights reserved.</p>
  </div>
)

export default Footer
