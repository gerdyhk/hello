// 设置不同环境的http请求的基础url
export const BASE_URL = {
  development: 'https://l94wc2001h.execute-api.ap-southeast-2.amazonaws.com',
  production: 'https://l94wc2001h.execute-api.ap-southeast-2.amazonaws.com',
}

// 设置http请求的默认配置
export const BASE_CONFIG = {
  defaults: {
    timeout: 6000,
  },
}
