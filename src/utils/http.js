import axios from 'axios'
import { BASE_URL, BASE_CONFIG } from '../config/http.config'

const env = process.env.NODE_ENV
const { defaults } = BASE_CONFIG
const http = axios.create({
  baseURL: BASE_URL[env],
  ...defaults,
})

http.interceptors.request.use(config => config, error => Promise.reject(error));


http.interceptors.response.use(response => {
  const { status, data } = response
  if (status === 200) {
    return data
  }
  return Promise.reject({
    message: `response statuscode is not 200:${status}`,
  })
}, error => Promise.reject(error));

export default http
