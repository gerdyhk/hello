import React, { useState } from 'react'
import Footer from '../../components/Footer'
import Header from '../../components/Header'
import Body from '../../components/Body'
import PopupForm from '../../components/PopupForm'
import styles from './index.styl'

const Home = () => {
  // 如果 isShowPopup 为真，则显示弹层
  const [isShowPopup, setPopup] = useState(false)
  const renderArr = [
    <div key="1" className={styles.wrapper}>
      <Header />
      {Body(setPopup)}
      <Footer />
    </div>]
  renderArr.push(PopupForm(isShowPopup, setPopup))
  return renderArr
}

export default Home
