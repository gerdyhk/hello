import React from 'react'
import ReactDOM from 'react-dom'
import Home from './pages/Home'

const Index = () => (
  <Home />
)


ReactDOM.render(<Index />, document.getElementById('index'))
