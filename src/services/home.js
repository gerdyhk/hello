import http from '../utils/http'

export function submitService(params) {
  return http.post('/prod/fake-auth', params)
}
