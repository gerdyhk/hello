## 代码说明

### 技术栈
* 前端框架：react + react hook
* css预处理: stylus
* css模块化: css modules
* http模块: axios
* 构建工具: webpack4 + babel7
* 代码规范: eslint + airbnb规范

### 说明
本代码基于react 16.8，使用了react hook替换 render props，可以较好的减小打包的体积。提升模块细粒度.使用css modules 防止样式污染。由于页面简单，没有启用router

### 使用说明

#### 目录结构
* components 组件
* config 一些业务相关的配置
* pages 页面模块
* services 与接口相关的请求配置
* utils 一些常用的utils模块
* dist 构建生成后的代码

#### 本地启动

* 首先 npm run init 安装依赖
* 使用 npm run start 启动本地预览
* 使用 npm run lint 检查代码规范
* 使用 npm run build 打包出生产环境所需的包


